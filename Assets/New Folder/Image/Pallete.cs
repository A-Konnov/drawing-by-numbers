﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pallete : MonoBehaviour
{
    [SerializeField] private Controller m_controller;

    public void SetColor()
    {
        m_controller.SelectedColor = gameObject.GetComponent<Image>().color;
        Debug.Log(m_controller.SelectedColor);
    }
}
