﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Red : MonoBehaviour
{
    [SerializeField] private Controller m_controller;

    public void FillRed()
    {
        if (m_controller.SelectedColor != Color.clear)
        {
            gameObject.GetComponent<Image>().color = m_controller.SelectedColor;
        }
    }
}
