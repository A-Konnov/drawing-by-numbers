﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{
    private Color m_selectedColor;

    public Color SelectedColor
    {
        get { return m_selectedColor; }
        set { m_selectedColor = value; }
    }

    private void Update()
    {
        RepaintOnClick();
    }

    private void RepaintOnClick()
    {
        if (Input.GetMouseButtonDown(0) && m_selectedColor != Color.clear)
        {
            RaycastHit2D hit = new RaycastHit2D();
            hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if(hit.collider != null)
            {
                GameObject target = hit.collider.gameObject;
                target.GetComponent<SpriteRenderer>().color = m_selectedColor;
            }
        }
    }
}
